package ru.arubtsova.myspringapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.arubtsova.myspringapp.dao.BookDAO;
import ru.arubtsova.myspringapp.model.Book;
import ru.arubtsova.myspringapp.service.BookService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/books")
public class BooksController {

    @Autowired
    private BookDAO bookDAO;

    //получим все книги и передадим на представление
    @GetMapping
    public String index(Model model) {
        model.addAttribute("books", bookDAO.index());
        return "books/allBooks";
    }

    @GetMapping("/findBooks")
    public String findBooks(@ModelAttribute("book") Book book) {
        return "books/findBooks";
    }

    @PostMapping("/findBooks")
    public String addBooks(@ModelAttribute("book") Book book) throws IOException {
        List<String> books = new ArrayList<>();
        if (!book.getAuthor().equals(""))
             books = BookService.sendRequest(book.getAuthor());

        System.out.println(books);
        bookDAO.saveAllBooks(books, book.getAuthor());
        return "redirect:/books";
    }

    //получаем книгу по айди и передаем на представление
    //PathVariable в адресе в аргументе будет передаваться айди и мы его заберем
    @GetMapping("/{id}")
    public String show(@PathVariable("id") int id, Model model) {
        model.addAttribute(bookDAO.show(id));
        return "books/show";
    }

    //форма для создания новой книги
    @GetMapping("/new")
    public String newBook(@ModelAttribute("book") Book book) {
        return "books/new";
    }

    //создание новой книги
    @PostMapping()
    public String create(@ModelAttribute("book") Book book) {
        bookDAO.save(book);
        return "redirect:/books";
    }

    //редактирование
    @GetMapping("/{id}/edit")
    public String edit(Model model, @ModelAttribute("id") int id) {
        model.addAttribute("book", bookDAO.show(id));
        return "books/edit";
    }

    //редактирование
    @PatchMapping("/{id}")
    public String update(@ModelAttribute("book") Book book, @ModelAttribute("id") int id) {
        bookDAO.update(id, book);
        return "redirect:/books";
    }

    //удаление
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") int id) {
        bookDAO.delete(id);
        return "redirect:/books";
    }

}
