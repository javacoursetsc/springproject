package ru.arubtsova.myspringapp.service;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.arubtsova.myspringapp.dao.BookDAO;

import java.io.IOException;
import java.util.*;

@Controller
public class BookService {



    public static void main(String[] args) throws IOException {


        List<String> books = BookService.sendRequest("игра престолов");
        System.out.println("Реквест " + books);

    }



    public static List<String> sendRequest(String stringToSearch) throws IOException {

        CloseableHttpClient httpClient = HttpClients.createDefault();

        try {

            // создаем объект клиента
            HttpGet request = new HttpGet("https://book24.ru/search/?q=" + stringToSearch.replace(" ", "%20"));
            //агата%20кристи
            CloseableHttpResponse response = httpClient.execute(request);

            try {

                // получаем статус ответа
                System.out.println(response.getProtocolVersion());              // HTTP/1.1
                System.out.println(response.getStatusLine().getStatusCode());   // 200
                System.out.println(response.getStatusLine().getReasonPhrase()); // OK
                System.out.println(response.getStatusLine().toString());        // HTTP/1.1 200 OK

                HttpEntity entity = response.getEntity();
                // если есть тело ответа
                if (entity != null) {
                    // возвращаем строку
                    String result = EntityUtils.toString(entity);
                    System.out.println(result);

                    return findAllBooks(result, stringToSearch);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                // закрываем соединения
                response.close();
            }
        } finally {
            httpClient.close();
        }
        return new ArrayList<>();
    }

    public static List<String> findAllBooks(String htmlDoc, String author) {

        Set<String> allBooks = new HashSet<>();
        List<String> allStrings = new ArrayList<>();

        Document html = Jsoup.parse(htmlDoc);
        Elements title = html.body().getElementsByTag("a");

        for (Element el : title) {
            allStrings.add(el.text());
        }

        for (int i = 0; i < allStrings.size(); i++) {
            if (allStrings.get(i).indexOf(author.substring(0, author.indexOf(" "))) > 0 ||
                    allStrings.get(i).indexOf(author.substring(author.indexOf(" ") + 1)) > 0) {
                allBooks.add(allStrings.get(i - 1));
            }
        }

        return new ArrayList<>(allBooks);
    }

}
