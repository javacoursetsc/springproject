package ru.arubtsova.myspringapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import ru.arubtsova.myspringapp.model.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class BookDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BookDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Book> index() {
        return jdbcTemplate.query("SELECT * FROM BOOKS", new BeanPropertyRowMapper<>(Book.class));
    }

    public void saveAllBooks(List<String> books, String author) {
        for (String book : books) {
            System.out.println(book);
            jdbcTemplate.update("INSERT INTO BOOKS(name, author, year) VALUES(?, ?, 2022)", book, author);
        }
    }

    public Book show(int id) {
        return jdbcTemplate.query("SELECT * FROM BOOKS WHERE id=?", new Object[]{id}, new BeanPropertyRowMapper<>(Book.class))
                .stream().findAny().orElse(null);
        //для такой простой логики можно вместо собственного маппера использовать встроенный
        //return jdbcTemplate.query("SELECT * FROM BOOKS WHERE id=?", new Object[]{id}, new BooksMapper()).stream().findAny().orElse(null);

        /* Старый код - просто для примера
        Book book = null;
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM BOOKS WHERE id=?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next(); //первая строка ResultSet
            book = new Book();
            book.setId(resultSet.getInt("id"));
            book.setName(resultSet.getString("name"));
            book.setAuthor(resultSet.getString("author"));
            book.setYear(resultSet.getInt("year"));
            return book;
        } catch (Exception e) {
        }
        return null;
     */
    }

    public void save(Book book) {
        jdbcTemplate.update("INSERT INTO BOOKS(name, author, year) VALUES(?, ?, ?)", book.getName(), book.getAuthor(), book.getYear());
        /*
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO BOOKS VALUES(1, ?, ?, ?)");
            statement.setString(1, book.getName());
            statement.setString(2, book.getAuthor());
            statement.setInt(3, book.getYear());
            statement.executeUpdate();
        } catch (Exception e) {
        }
         */
    }

    public void update(int id, Book updateBook) {
        jdbcTemplate.update("UPDATE BOOKS SET name = ?, author = ?, year = ? WHERE id= ?", updateBook.getName(), updateBook.getAuthor(), updateBook.getYear(), updateBook.getId());

        /*
        Book book = null;
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE BOOKS SET name = ?, author = ?, year = ? WHERE id= ?");
            statement.setString(1, updateBook.getName());
            statement.setString(2, updateBook.getAuthor());
            statement.setInt(3, updateBook.getYear());
            statement.setInt(4, updateBook.getId());

            statement.executeUpdate();
        } catch (Exception e) {
        }
         */
    }

    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM BOOKS WHERE id= ?", id);

        /*
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM BOOKS WHERE id= ?");
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
        }
         */
    }

}
